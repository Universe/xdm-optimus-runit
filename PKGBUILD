# Maintainer: linuxer <linuxer@artixlinux.org>
# Contributor: Muhammad Herdiansyah <herdiansyah@netc.eu>

_sed_args=(-e 's|/var/service|/run/runit/service|g' -e 's|/var/run|/run|g' -e 's|/usr/sbin|/usr/bin|g' -e 's|/opt/bin|/usr/bin|g' -e 's|/usr/libexec|/usr/lib|g')

pkgname=xdm-optimus-runit
pkgver=20210323
pkgrel=1
pkgdesc="runit service scripts for optimus xdm (The original Xorg XDM)"
arch=('any')
url="https://gitea.artixlinux.org/linuxer/Artix-Optimus"
license=('BSD3')
# Note: While this PKGBUILD is licensed under BSD-3 terms, all of the
#       included runscript should follow it's main package's licenses.
groups=('runit-world')
depends=('xorg-xdm')
conflicts=('systemd-sysvcompat' 'xdm-runit')
provides=('init-gdm' 'init-displaymanager-runit' 'init-displaymanager')
source=("xdm.run")
sha256sums=('407ef3ac05371c989668a16d068798f5a3af622ba1a93cc5db2b027766e71053')

_inst_logsv() {
    for file in run finish check; do
        if test -f "$srcdir/log$1.$file"; then
            install -Dm755 "$srcdir/log$1.$file" "$pkgdir/etc/runit/sv/$1/log/$file"
            sed "${_sed_args[@]}" -i "$pkgdir/etc/runit/sv/$1/log/$file"
        fi
    done
}

_inst_sv() {
    if test -f "$srcdir/$1.conf"; then
        install -Dm644 "$srcdir/$1.conf" "$pkgdir/etc/runit/sv/$1/conf"
    fi

    for file in run finish check; do
        if test -f "$srcdir/$1.$file"; then
            install -Dm755 "$srcdir/$1.$file" "$pkgdir/etc/runit/sv/$1/$file"
            sed "${_sed_args[@]}" -i "$pkgdir/etc/runit/sv/$1/$file"
        fi
    done
}

package() {
    _inst_sv 'xdm'
    sed -i "2i \ \ \ \  sv check optimus-manager > /dev/null || exit 1" "$pkgdir/etc/runit/sv/xdm/run"
}
